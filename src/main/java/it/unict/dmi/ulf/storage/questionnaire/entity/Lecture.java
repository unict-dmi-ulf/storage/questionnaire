package it.unict.dmi.ulf.storage.questionnaire.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
public class Lecture {
    @Id
    @GeneratedValue
    long id;

    Date date;

    @ManyToOne
    @NotNull
    Questionnaire questionnaire;
}
