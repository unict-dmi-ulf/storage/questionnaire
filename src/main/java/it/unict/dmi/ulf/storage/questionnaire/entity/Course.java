package it.unict.dmi.ulf.storage.questionnaire.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
public class Course {
    @Id
    @GeneratedValue
    long id;

    String name;

    @OneToMany(cascade = CascadeType.ALL)
    Collection<Lecture> lectures;
}
