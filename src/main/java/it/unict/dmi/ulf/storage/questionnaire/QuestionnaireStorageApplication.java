package it.unict.dmi.ulf.storage.questionnaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionnaireStorageApplication {
	public static void main(String[] args) {
		SpringApplication.run(QuestionnaireStorageApplication.class, args);
	}

}
