package it.unict.dmi.ulf.storage.questionnaire.entity.embeddable;

public enum QuestionType {
    OPEN_RESPONSE, RATE_5; 
}
