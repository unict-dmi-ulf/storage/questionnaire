package it.unict.dmi.ulf.storage.questionnaire.repository;

import org.springframework.data.repository.CrudRepository;

import it.unict.dmi.ulf.storage.questionnaire.entity.Questionnaire;

public interface QuestionnaireRepository extends CrudRepository<Questionnaire, Long> {
}
