package it.unict.dmi.ulf.storage.questionnaire.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import it.unict.dmi.ulf.storage.questionnaire.entity.embeddable.QuestionType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
public class Question {
    @Id
    @GeneratedValue
    long id;

    QuestionType type;
    String text;
}
