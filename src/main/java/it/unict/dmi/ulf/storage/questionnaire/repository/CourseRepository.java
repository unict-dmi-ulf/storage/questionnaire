package it.unict.dmi.ulf.storage.questionnaire.repository;

import org.springframework.data.repository.CrudRepository;

import it.unict.dmi.ulf.storage.questionnaire.entity.Course;

public interface CourseRepository extends CrudRepository<Course, Long> {
}
