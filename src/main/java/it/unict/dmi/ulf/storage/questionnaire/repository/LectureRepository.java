package it.unict.dmi.ulf.storage.questionnaire.repository;

import org.springframework.data.repository.CrudRepository;

import it.unict.dmi.ulf.storage.questionnaire.entity.Lecture;

public interface LectureRepository extends CrudRepository<Lecture, Long> {
}
